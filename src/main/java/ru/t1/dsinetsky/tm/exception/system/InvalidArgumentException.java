package ru.t1.dsinetsky.tm.exception.system;

public final class InvalidArgumentException extends GeneralSystemException {

    public InvalidArgumentException() {
        super("Invalid argument! Use argument \"-h\" for list of arguments!");
    }

    public InvalidArgumentException(final String message) {
        super("Argument " + message + " is not supported! Use argument \"-h\" for list of arguments!");
    }
}
