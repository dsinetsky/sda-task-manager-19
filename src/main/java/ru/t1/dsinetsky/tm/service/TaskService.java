package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;
import java.util.Random;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) throws GeneralException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) throws GeneralException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findById(id);
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final int index, final Status status) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize()) throw new IndexOutOfSizeException(getSize());
        final Task task = findByIndex(index);
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> returnTasksOfProject(final String projectId) throws GeneralFieldException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        return repository.findTasksByProjectId(projectId);
    }

    @Override
    public void createTest() throws GeneralException {
        final Random randomizer = new Random();
        final String name = "task";
        final String description = "desc";
        for (int i = 1; i < 11; i++) {
            final Task task = create(name + randomizer.nextInt(11), description + randomizer.nextInt(11));
            final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            task.setStatus(status);
        }
    }

}
