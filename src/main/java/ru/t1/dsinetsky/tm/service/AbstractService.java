package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.api.service.IAbstractService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.EntityIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    protected R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    //get name of model for exceptions
    private String getModelType() {
        //first, get repository class name
        final String repositoryClass = repository.getClass().getName();
        //second, get index of last dot in class name. Should increase index by 1 to avoid dot
        final int indexStart = repositoryClass.lastIndexOf(".") + 1;
        //third, remove last "Repository" charset from class name
        final int indexEnd = repositoryClass.length() - 10;
        //finally, get and return just name of model as string
        return repositoryClass.substring(indexStart, indexEnd);
    }

    @Override
    public List<M> returnAll() {
        return repository.returnAll();
    }

    @Override
    public List<M> returnAll(final Comparator comparator) {
        if (comparator == null) return returnAll();
        return repository.returnAll(comparator);
    }

    @Override
    public List<M> returnAll(final Sort sort) {
        if (sort == null) return returnAll();
        return repository.returnAll(sort.getComparator());
    }

    @Override
    public M add(final M model) throws GeneralException {
        if (model == null) throw new EntityNotFoundException(getModelType());
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        final M model = repository.findById(id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    public M findByIndex(final int index) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize()) throw new IndexOutOfSizeException(repository.getSize());
        return repository.findByIndex(index);
    }

    @Override
    public M removeById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        final M model = repository.removeById(id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    public M removeByIndex(final int index) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize()) throw new IndexOutOfSizeException(repository.getSize());
        return repository.removeByIndex(index);
    }

    @Override
    public boolean existsById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
