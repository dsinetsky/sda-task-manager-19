package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @Override
    public User create(final String login, final String password) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String firstName, final String lastName, final String middleName) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String email) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .email(email)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final String email, final String firstName, final String lastName, final String middleName) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final Role role, final String email, final String firstName, final String lastName, final String middleName) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws GeneralException {
        final User newUser = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .toUser();
        this.add(newUser);
        return newUser;
    }

    @Override
    public User add(final User newUser) throws GeneralException {
        if (newUser == null) throw new InvalidUserException();
        final String login = newUser.getLogin();
        if (login == null || login.isEmpty()) throw new IncorrectLoginPasswordException();
        if (isUserExistByLogin(login)) throw new UserAlreadyExistException();
        final String password = newUser.getPasswordHash();
        if (password == null || password.isEmpty()) throw new IncorrectLoginPasswordException();
        final Role role = newUser.getRole();
        if (role == null) throw new RoleIsEmptyException();
        newUser.setPasswordHash(HashUtil.salt(password));
        repository.add(newUser);
        return newUser;
    }

    @Override
    public void removeUserByLogin(final String login) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        repository.removeByLogin(login);
    }

    @Override
    public User findUserByLogin(final String login) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        return repository.findUserByLogin(login);
    }

    @Override
    public User updateUserById(final String id, final String firstName, final String lastName, final String middleName) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        final User user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateUserByLogin(final String login, final String firstName, final String lastName, final String middleName) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        final User user = findUserByLogin(login);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateEmailById(final String id, final String email) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        final User user = findById(id);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateEmailByLogin(final String login, final String email) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        final User user = findUserByLogin(login);
        user.setEmail(email);
        return user;
    }

    @Override
    public User changePassword(final String id, final String password) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public boolean isUserExistByLogin(final String login) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        return repository.isUserLoginExist(login);
    }

    @Override
    public void createTest() throws GeneralException {
        create("admin", "admin", Role.ADMIN);
        create("dsinetsky", "change", "dsinetsky@t1-consulting.ru");
        this.add(UserBuilder.create()
                .login("user")
                .password("pass")
                .email("user-email@gmail.com")
                .firstName("Ivan")
                .lastName("Ivanov")
                .middleName("Ivanovich")
                .role(Role.USUAL)
                .toUser()
        );
    }

}
