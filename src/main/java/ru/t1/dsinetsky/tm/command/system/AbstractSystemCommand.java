package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

import java.util.Collection;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    protected void listCommands(Collection<AbstractCommand> commands) {
        for (final ICommand command : commands) {
            System.out.println(command);
        }
    }

}
