package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class SystemHelpDisplayCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = null;

    public static final String NAME = TerminalConst.CMD_SYSTEM_HELP;

    public static final String DESCRIPTION = "Shows system commands";

    @Override
    public void execute() {
        listCommands(getCommandService().getSystemCommands());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
