package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_PROJECT_LIST;

    public static final String DESCRIPTION = "Show available projects";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> listProjects = getProjectService().returnAll(sort);
        System.out.println("List of projects:");
        int index = 1;
        for (final Project project : listProjects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
