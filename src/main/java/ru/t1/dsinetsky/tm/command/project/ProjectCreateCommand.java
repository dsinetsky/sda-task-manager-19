package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = TerminalConst.CMD_PROJECT_CREATE;

    public static final String DESCRIPTION = "Create new project";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter name of new project:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
        System.out.println("Project successfully created!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
