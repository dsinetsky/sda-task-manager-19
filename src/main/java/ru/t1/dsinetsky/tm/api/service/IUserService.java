package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IUserService extends IAbstractService<User> {

    User create(String login, String password) throws GeneralException;

    User create(String login, String password, Role role) throws GeneralException;

    User create(String login, String password, String email) throws GeneralException;

    User create(String login, String password, String firstName, String lastName, String middleName) throws GeneralException;

    User create(String login, String password, String email, String firstName, String lastName, String middleName) throws GeneralException;

    User create(String login, String password, Role role, String email, String firstName, String lastName, String middleName) throws GeneralException;

    User add(User newUser) throws GeneralException;

    void removeUserByLogin(String login) throws GeneralUserException;

    User findUserByLogin(String login) throws GeneralUserException;

    User updateUserById(String id, String firstName, String lastName, String middleName) throws GeneralException;

    User updateUserByLogin(String login, String firstName, String lastName, String middleName) throws GeneralUserException;

    User updateEmailById(String id, String email) throws GeneralException;

    User updateEmailByLogin(String login, String email) throws GeneralUserException;

    User changePassword(String id, String password) throws GeneralException;

    List<User> returnAll();

    boolean isUserExistByLogin(String login) throws GeneralUserException;

}
