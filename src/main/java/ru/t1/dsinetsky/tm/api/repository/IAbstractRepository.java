package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M> {

    List<M> returnAll();

    List<M> returnAll(Comparator comparator);

    M add(M model) throws GeneralException;

    void clear();

    M findById(String id) throws GeneralException;

    M findByIndex(int index) throws GeneralException;

    M removeById(String id) throws GeneralException;

    M removeByIndex(int index) throws GeneralException;

    boolean existsById(String id) throws GeneralException;

    int getSize();

}
