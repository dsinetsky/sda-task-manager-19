package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    Task create(String name) throws GeneralException;

    Task create(String name, String desc) throws GeneralException;

    List<Task> returnTasksOfProject(String projectId) throws GeneralException;

    Task updateById(String id, String name, String desc) throws GeneralException;

    Task updateByIndex(int index, String name, String desc) throws GeneralException;

    Task changeStatusById(String id, Status status) throws GeneralException;

    Task changeStatusByIndex(int index, Status status) throws GeneralException;

}
