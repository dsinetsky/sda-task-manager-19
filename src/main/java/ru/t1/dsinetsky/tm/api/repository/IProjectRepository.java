package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectRepository extends IAbstractRepository<Project> {

    Project create(String name);

    Project create(String name, String desc);

}
