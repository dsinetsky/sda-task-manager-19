package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractRepository<Task> {

    Task create(String name);

    Task create(String name, String desc);

    List<Task> findTasksByProjectId(String projectId);

}
