package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectService extends IAbstractService<Project> {

    Project create(String name) throws GeneralException;

    Project create(String name, String desc) throws GeneralException;

    Project updateById(String id, String name, String desc) throws GeneralException;

    Project updateByIndex(int index, String name, String desc) throws GeneralException;

    Project changeStatusById(String id, Status status) throws GeneralException;

    Project changeStatusByIndex(int index, Status status) throws GeneralException;

}
